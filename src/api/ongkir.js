import axios from "./axios";

export default {
  GetAllProvince: () => axios.get("/api/provinsi"),
  GetCityByProvinceId: (id) => axios.get("/api/kota/" + id),
  GetAllCourir: () => axios.get("/user/getAllCourir"),
  SubmitOngkir: (payload) => axios.post("/api/ongkir", payload),
};
