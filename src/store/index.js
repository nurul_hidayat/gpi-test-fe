import Vuex from "vuex";
import OngkirModule from "./Ongkir";

const store = new Vuex.Store({
  modules: {
    ongkir: OngkirModule,
  },
});

export default store;
