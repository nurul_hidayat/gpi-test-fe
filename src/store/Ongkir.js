import ongkir from "./../api/ongkir";

const Ongkir = {
  state: {
    province: ["Please select province"],
    originCity: [],
    destinationCity: [],
    originSubdistrict: [],
    destinationSubdistrict: [],
    courier: [],
    courierName: "",
    ongkir: {},
    isSuccess: false,
    errorMessage: "",
    loading: false,
  },
  mutations: {
    setLoading(state, payload) {
      state.loading = payload;
    },
    setProvince(state, payload) {
      state.province = payload;
    },
    setOriginCity(state, payload) {
      state.originCity = payload;
    },
    setDestinationCity(state, payload) {
      state.destinationCity = payload;
    },
    setCourier(state, payload) {
      state.courier = payload;
    },
    setCourierName(state, payload) {
      state.courierName = payload;
    },
    setOngkir(state, payload) {
      state.ongkir = payload;
    },
    setIsSuccess(state, payload) {
      state.isSuccess = payload;
    },
    setErrorMessage(state, payload) {
      state.errorMessage = payload;
    },
  },
  actions: {
    async getProvince({ commit }) {
      let provinceCache = localStorage.getItem("provinces");
      if (provinceCache) {
        commit("setProvince", JSON.parse(provinceCache));
      } else {
        const response = await ongkir.GetAllProvince();
        if (response.rajaongkir.status.code === 200) {
          let data = response.rajaongkir.results;
          localStorage.setItem("provinces", JSON.stringify(data));
          commit("setProvince", data);
        } else {
          commit(
            "setErrorMessage",
            "Failed get data Provinces, please refresh the page and try again."
          );
        }
      }
    },
    async getCity({ commit }, payload) {
      const response = await ongkir.GetCityByProvinceId(payload.id);
      console.log("getCity:", payload);
      if (response.rajaongkir?.status?.code === 200) {
        let data = response.rajaongkir.results;
        if (payload.name === "destinationProvince") {
          commit("setDestinationCity", data);
        } else {
          commit("setOriginCity", data);
        }
      } else {
        if (payload.name === "destinationProvince") {
          commit(
            "setErrorMessage",
            "Failed get data destination city, please select another province or refresh the page."
          );
        } else {
          commit(
            "setErrorMessage",
            "Failed get data origin city, please select another province or refresh the page."
          );
        }
      }
    },
    async getCourier({ commit }) {
      const response = await ongkir.GetAllCourir();
      if (response.status === 200) {
        commit("setCourier", response.data);
      }
    },
    async submitOngkir({ commit }, payload) {
      commit("setLoading", true);
      const response = await ongkir.SubmitOngkir(payload);
      if (response.rajaongkir?.status?.code === 200) {
        commit("setCourierName", response.rajaongkir?.results[0].name);
        console.log(response.rajaongkir?.results[0].costs);
        commit("setOngkir", response.rajaongkir?.results[0]);
        commit("setIsSuccess", true);
      } else {
        commit("setIsSuccess", false);
        commit(
          "setErrorMessage",
          "System is busy right now, please re-click check button."
        );
      }
      commit("setLoading", false);
    },
    resetErrorMessage({ commit }, payload) {
      commit("setErrorMessage", payload);
    },
  },
  getters: {
    // getProvince: (state) => state.province,
    // getCity: (state) => state.city,
    // getCourier: (state) => state.courier,
  },
};
export default Ongkir;
